#pragma once

#include "ofMain.h"
#include "ofxHapPlayer.h"
#include "ofxEyeFace.h"
#include <Poco/ThreadTarget.h>
#include <Poco/RWLock.h>
#include "ofxXmlSettings.h"
#include "ofxGui.h"

#define VEL_MAX 50

class Detector : public ofThread {
public:
	Detector () { started = false; locked = false;}

	void start() {
		started = false;
		startThread();
	}

	std::function<void()> startup;
	std::function<void()> lambda;
	//auto say_hello = [](){ std::cout << "Hello"; };

	bool locked;

	void (*func)();

		    void threadedFunction()
			{
				startup();
				while(isThreadRunning())
					{
						lambda();
					}
			}

	bool started;

};

class ofApp : public ofBaseApp, public ofThread{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h){};
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void exit() {
			grabber.close();
		}

		// Settings:
		ofxXmlSettings settings;
		ofPoint camGrabSize, camDetectionSize, videoOverlaySize, videoOverlayOffset;
		int rotateCam; // 0 = no rotation
					   // 1 = +90 deg
					   // 2 = -90 deg
		float camDrawScale;
		int switchArea;
		float followSpeed;
		float slowdownFactor;

		ofPoint windowPosition, windowSize;


		struct option {
			int minAge, maxAge;
			int gender; // 0 = male, 1 = female 
			ofxHapPlayer player;
			string filename;
		};
		vector <option*> options;

		// App vars:
		ofPoint current_rect_vector;
		ofRectangle current_rect;
		int current_area;
		int current_age, current_gender; // 0 = male, 1 = female 
		float switchVar;
		int currentOption;

		ofFbo camDetectionFbo;
		Detector detector;

		void hi() {
			ofLog() << "hi!";
		}
		int upcounter;

		bool debug;

		void processEyeFace();

		ofPoint detectionArea;
		
		ofVideoGrabber grabber;
		ofPixels grabberPixels;

		ofxHapPlayer player, comecloser;

		ofxEyeFace eyeFace;


		// UI
		ofxPanel gui;
		ofxFloatSlider camDrawScaleSlider;
		ofxIntSlider videoOverlayOffsetX, videoOverlayOffsetY,
			videoOverlaySizeX, videoOverlaySizeY,
			windowPositionX, windowPositionY,
			windowSizeX, windowSizeY;
		ofxFloatSlider followSpeedSlider, slowdownFactorSlider;

		float ui_scale;
		float energy;
		int fps_slider;
		double lastTimeDone;

		ofPoint nominalPlayerSize;

		float velocity, velocity_target;
};
