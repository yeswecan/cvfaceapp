#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	player.loadMovie("Layout_R8.mov");
	player.play();
	player.setLoopState(OF_LOOP_NORMAL);

	// Setting up from settings:
	settings.load("settings.xml");
	camGrabSize = ofPoint(settings.getAttribute("camGrabSize", "x", 0, 0),
						  settings.getAttribute("camGrabSize", "y", 0, 0));
	camDetectionSize = ofPoint(settings.getAttribute("camDetectionSize", "x", 0, 0),
						  settings.getAttribute("camDetectionSize", "y", 0, 0));
	videoOverlaySize = ofPoint(settings.getAttribute("videoOverlaySize", "x", 0, 0),
						  settings.getAttribute("videoOverlaySize", "y", 0, 0));
	videoOverlayOffset = ofPoint(settings.getAttribute("videoOverlayOffset", "x", 0, 0),
						  settings.getAttribute("videoOverlayOffset", "y", 0, 0));

	windowPosition = ofPoint(settings.getAttribute("windowPosition", "x", 0, 0),
						  settings.getAttribute("windowPosition", "y", 0, 0));
	windowSize = ofPoint(settings.getAttribute("windowSize", "x", 1024, 0),
						  settings.getAttribute("windowSize", "y", 960, 0));

	rotateCam = settings.getValue("rotateCam", 0, 0);
	slowdownFactor = settings.getValue("slowdownFactor", 0., 0);
	camDrawScale = settings.getValue("camDrawScale", 0., 0);
	//camDrawScaleY = settings.getValue("camDrawScaleY", 0., 0);
	switchArea = settings.getValue("switchArea", 4000, 0);
	followSpeed = settings.getValue("followSpeed", 1.5, 0);
	ofLog() << "camGrabSize:" << camGrabSize;
	ofLog() << "camDetectionSize:" << camDetectionSize;
	ofLog() << "videoOverlaySize:" << videoOverlaySize;
	ofLog() << "videoOverlayOffset:" << videoOverlayOffset;
	ofLog() << "rotateCam:" << rotateCam;
	ofLog() << "camDrawScale:" << camDrawScale;
	ofLog() << "switchArea:" << switchArea;
	ofLog() << "followSpeed:" << followSpeed;
	ofLog() << "slowdownFactor:" << slowdownFactor;
	ofLog() << "windowPosition:" << windowPosition;
	ofLog() << "windowSize:" << windowSize;

//	camDrawScale = ofGetScreenWidth() / camDetectionSize.x;

	ofLog() << "comecloser:" << settings.getValue("comecloser", "", 0);
	comecloser.loadMovie(settings.getValue("comecloser", "", 0));
	comecloser.play();
	comecloser.setLoopState(OF_LOOP_NORMAL);	
	ofLog() << "options count:" << settings.getNumTags("option");
	for (int i = 0; i < settings.getNumTags("option"); i++) {
		option *addon = new option;
		ofLog() << "loading movie:" << settings.getValue("option", "", i);
		addon->player.loadMovie(settings.getValue("option", "", i));
		addon->player.play();
		addon->player.setLoopState(OF_LOOP_NORMAL);		
		addon->gender = settings.getAttribute("option", "gender", 0, i);
		addon->minAge = settings.getAttribute("option", "minAge", 0, i);
		addon->maxAge = settings.getAttribute("option", "maxAge", 0, i);
		addon->filename = settings.getValue("option", "", i);
/*		ofLog() << "pushing option " << i+1 << ":" << settings.getValue("option", "", i)
			<< " : " << addon.minAge << " - " << addon.maxAge << " years";*/
		options.push_back(addon);
		ofLog() << "pushed";
	}
	ofLog() << "finished loading vids";
/*	settings.clear();
	settings.addTag("camGrabSize");
	settings.addAttribute("camGrabSize", "x", 640, 0);
	settings.addAttribute("camGrabSize", "y", 480, 0);
	settings.addTag("camDetectionSize");
	settings.addTag("camOverlaySize");
	settings.addTag("camOverlayOffset");
	settings.addTag("camDrawScale");
	settings.addValue("rotateCam", 0);
	settings.addValue("switchArea", 4000);
	settings.saveFile("settings.xml");*/

	// (fake settings)
/*	camGrabSize = ofPoint(640, 480);
	camDetectionSize = ofPoint(320, 240);
	videoOverlaySize = ofPoint(320, 200);
	videoOverlayOffset = ofPoint(-130, -100);
	rotateCam = 1;
	camDrawScale = 3.;
	switchArea = 4000;*/

	debug = false;

	switch (rotateCam) {
	case 0:
		camDetectionFbo.allocate(camDetectionSize.x, camDetectionSize.y, GL_RGB);
		break;
	case 1:
		camDetectionFbo.allocate(camDetectionSize.y, camDetectionSize.x, GL_RGB);
		break;
	case 2:
		camDetectionFbo.allocate(camDetectionSize.y, camDetectionSize.x, GL_RGB);
		break;
	}

	//detectionArea = ofPoint(640,480);

	int camWidth 		= camGrabSize.x;	// try to grab at this size. 
	int camHeight 		= camGrabSize.y;
	
    //we can now get back a list of devices. 
	vector<ofVideoDevice> devices = grabber.listDevices();
	
    for(int i = 0; i < devices.size(); i++){
		cout << devices[i].id << ": " << devices[i].deviceName; 
        if( devices[i].bAvailable ){
            cout << endl;
        }else{
            cout << " - unavailable " << endl; 
        }
	}
    
	grabber.setDeviceID(settings.getValue("cameraIndex", 0, 0));
	grabber.setDesiredFrameRate(30);
	grabber.initGrabber(camWidth,camHeight);

	ofSetFrameRate(30);
	//ofLog() << "the final wait...";
	//ofSleepMillis(3000);

	//ui_scale = (float)ofGetWidth() / camWidth;

	fps_slider = 3;
	

	gui.setup();
	gui.add(camDrawScaleSlider.setup("camDrawScale", camDrawScale, 0.1, 10., 500, 50));
	gui.add(videoOverlayOffsetX.setup("videoOverlayOffsetX", videoOverlayOffset.x, -300, 300, 500, 50)); 
	gui.add(videoOverlayOffsetY.setup("videoOverlayOffsetY", videoOverlayOffset.y, -300, 300, 500, 50)); 
	gui.add(videoOverlaySizeX.setup("videoOverlaySizeX", videoOverlaySize.x, -300, 800, 500, 50)); 
	gui.add(videoOverlaySizeY.setup("videoOverlaySizeY", videoOverlaySize.y, -300, 800, 500, 50)); 
	gui.add(followSpeedSlider.setup("followSpeed", followSpeed, 0.1, 10., 500, 50));
	gui.add(slowdownFactorSlider.setup("slowdownFactor", slowdownFactor, 0.1, 10., 500, 50));

	//gui.add(windowPositionX.setup("windowPositionX", windowPosition.x, -50, 3000, 500, 50));
	gui.add(windowPositionY.setup("windowPositionY", windowPosition.y, -50, 3000, 500, 50));
	gui.add(windowSizeX.setup("windowSizeX", windowSize.x, -50, 3000, 500, 50));
	gui.add(windowSizeY.setup("windowSizeY", windowSize.y, -50, 3000, 500, 50));
	/*
	detector.startup = [&]() {
		eyeFace.setup();
	};
	detector.lambda = [&]() {
		//ofLog() << "hi!!!";
		processEyeFace();
	};*/
	//detector.start();

	eyeFace.setup();

	upcounter = 0;

	currentOption = 0;
}

//--------------------------------------------------------------
void ofApp::update(){
	// UI:
	camDrawScale = camDrawScaleSlider;
	videoOverlayOffset.x = videoOverlayOffsetX;
	videoOverlayOffset.y = videoOverlayOffsetY;
	videoOverlaySize.x = videoOverlaySizeX;
	videoOverlaySize.y = videoOverlaySizeY;

	
	windowPosition = ofPoint(windowPosition.x, windowPositionY);
	windowSize = ofPoint(windowSizeX, windowSizeY);
	ofLog() << "setting window position to " << windowPosition;
	ofLog() << "setting window size to " << windowSize;
	ofSetWindowPosition(windowPosition.x, windowPosition.y);
	ofSetWindowShape(windowSize.x, windowSize.y);

	// Inner logic:
	upcounter++;
	grabber.update();
	options[currentOption]->player.update();
	if (current_area <= switchArea) {
		comecloser.update();
	}
	/*
	if ((ofGetElapsedTimef() > 3) && (!detector.started)) {
		detector.start();
	}*/

	if ((grabber.isFrameNew()) && (!detector.locked)) {
		//ofLog() << "frame is new, drawing from grabber...";
		camDetectionFbo.begin();
		ofSetColor(255);
		if (rotateCam == 0)
			grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
		else if (rotateCam == 1) {
			ofPushMatrix();
				ofTranslate(camDetectionSize.y, 0);
				ofRotateZ(90);
				grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
			ofPopMatrix();
		} else if (rotateCam == 2) {
			ofPushMatrix();
				ofTranslate(0, camDetectionSize.x);
				ofRotateZ(-90);
				grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
			ofPopMatrix();
		}
		camDetectionFbo.end();

		camDetectionFbo.getTextureReference().readToPixels(grabberPixels);
			processEyeFace();
	}

	current_area = current_rect.width * current_rect.height;
	if ((current_rect.width * current_rect.height) > switchArea) {
		for (int i = 0; i < options.size(); i++) {
			if ((options[i]->gender == current_gender) && 
				(current_age > options[i]->minAge) && (current_age < options[i]->maxAge)) {
					currentOption = i;
			}
		}
	}
	//ofLog() << "update ends";
}

void ofApp::processEyeFace() {
	int targetfps = fps_slider;
	float timeframe = 1. / (float)targetfps;

	if ((ofGetElapsedTimef() - lastTimeDone) >= timeframe) {
		detector.locked = true;
		//ofLog() << "I'm running!";
		eyeFace.min_stability = 0.5;
		eyeFace.visible_at_least = 0.8;
			//ofLog() << "preparing to process pixels...";
			eyeFace.processPixels(grabberPixels);
			//ofLog() << "processed pixels!";
		lastTimeDone = ofGetElapsedTimef();
		//ofLog() << "people found:" << eyeFace.results.size();
/*		if (eyeFace.results.size() > 0) {
			ofLog() << "x:" << eyeFace.results[0]->current_position.position.x <<
				" y:" << eyeFace.results[0]->current_position.position.y;
		}*/
		detector.locked = false;
	}

	//ofLog() << "analyzing results...";
	int biggestResult = -1;
	int biggestResultSize = 0;
	if (eyeFace.results.size() > 0) { //finding the biggest result
		for (int i = 0; i < eyeFace.results.size(); i++) {
			if ((eyeFace.results[i]->current_position.width * 
				eyeFace.results[i]->current_position.height) > biggestResultSize) {
					biggestResultSize = eyeFace.results[i]->current_position.width * 
				eyeFace.results[i]->current_position.height;
					biggestResult = i;
			}
		}
	}
	if ((biggestResult > -1) &&
		 ((eyeFace.results.size() > 0) && (eyeFace.results[biggestResult]->current_position.position.x >= 0) && 
		(eyeFace.results[biggestResult]->current_position.position.x <= camDetectionSize.x))) {
		velocity_target =	ofClamp((eyeFace.results[biggestResult]->current_position.position - current_rect.position).length(), 0, VEL_MAX);
		velocity += (velocity_target - velocity) / 200;
		//ofLog() << "velocity:" << velocity << " ; target:" << velocity_target;

		float normalizedVelocity = ((float)ofClamp((velocity > 4 ? velocity - 4 : 0), 0, 20) / 20.);

		current_rect_vector = ((eyeFace.results[biggestResult]->current_position.position - current_rect.position) / 20);
		current_rect.position += current_rect_vector * normalizedVelocity * followSpeed;
		current_rect.width += ((eyeFace.results[biggestResult]->current_position.width - current_rect.width) / 20) * normalizedVelocity;
		current_rect.height += ((eyeFace.results[biggestResult]->current_position.height - current_rect.height) / 20) * normalizedVelocity;
		energy += ((float)eyeFace.results[biggestResult]->energy - energy) / 10;
		current_age = eyeFace.results[biggestResult]->age;
		current_gender = (eyeFace.results[biggestResult]->gender == EF_GENDER_MALE ? 0 : 1);
	} else { 
		energy -= energy / 120;
		velocity -= velocity / 200;
		float normalizedVelocity = ((float)ofClamp((velocity > 4 ? velocity - 4 : 0), 0, 20) / 20.);
		current_rect.position += current_rect_vector * normalizedVelocity * followSpeed;
	}

	if (((current_rect.width * current_rect.height) > switchArea ) && (energy > 0.5)) {
		(switchVar < 1 ? switchVar += 0.01 : switchVar);
	} else {
		(switchVar > 0 ? switchVar -= 0.01 : switchVar);
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(0);
	ofSetColor(255);
	
	ofPushMatrix();
	switch (rotateCam) {
	case 0:
		ofScale(camDrawScale, camDrawScale);
		grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
		break;
	case 1:
		ofTranslate(camDetectionSize.y * camDrawScale, 0);
		ofRotateZ(90);
		ofScale(camDrawScale, camDrawScale);
		grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
		break;
	case 2:
		ofTranslate(0, camDetectionSize.x * camDrawScale);
		ofRotateZ(-90);
		ofScale(camDrawScale, camDrawScale);
		grabber.draw(0,0, camDetectionSize.x, camDetectionSize.y);
		break;
	}
	ofPopMatrix();
	//grabber.draw(0,0, camGrabSize.x, camGrabSize.y);
	
	nominalPlayerSize = ofPoint(player.getWidth() / 3, player.getHeight() / 3);
	ofPoint offset = ofPoint(100, 50);
	ofPushMatrix();
		ofScale(camDrawScale, camDrawScale);
		ofSetColor(255, 255 /** energy*/ * (1 - switchVar));
		comecloser.draw(0,0, camDetectionSize.x, camDetectionSize.y);

		ofSetColor(255, 255 * energy * switchVar);
		/*player.draw(current_rect.x - nominalPlayerSize.x / 2 + offset.x, current_rect.y - nominalPlayerSize.y / 2 + offset.y,
		nominalPlayerSize.x, nominalPlayerSize.y);*/
		ofTranslate(current_rect.x, current_rect.y);
		options[currentOption]->player.draw(0 + videoOverlayOffset.x,0 + videoOverlayOffset.y,videoOverlaySize.x, videoOverlaySize.y);
	ofPopMatrix();
	

	if (debug) {
		ofSetColor(0, 120);
		ofRect(0,0, ofGetWidth(), ofGetHeight());
		ofSetColor(255);
		//grabber.draw(0,0);
		camDetectionFbo.draw(640,0);

		ofPushMatrix();
		ofScale(camDrawScale, camDrawScale);
			ofSetColor(255, 0, 255, 100 * energy);
			ofRect(current_rect.x,
				current_rect.y,
				current_rect.width,
				current_rect.height);
			ofSetColor(255);
			ofDrawBitmapString(ofToString(current_age) + ", " + 
				(current_gender == 0 ? "male " : "female ") + ", area:" +
				ofToString(current_rect.width * current_rect.height) +
				
				"\n" + options[currentOption]->filename, current_rect.x, current_rect.y);
		ofPopMatrix();
		ofDrawBitmapString(ofToString(eyeFace.results.size()) + " faces detected", 50, 50);
		ofDrawBitmapString(ofToString(energy) + " energy", 50, 150);
		ofDrawBitmapString(ofToString(switchVar) + " switch", 50, 100);

		gui.draw();
	}
	/*
	if (rotateCam = 0)
		ofLine(ofPoint(640,0), ofPoint(640+ camDetectionFbo.getWidth(), camDetectionFbo.getHeight()));
	else if (rotateCam != 0){
		ofLine(ofPoint(640,0), ofPoint(640 + camDetectionFbo.getHeight(), camDetectionFbo.getWidth()));
	}*/

/*
	ofSetColor(255);
	for (int i = 0; i < eyeFace.results.size(); i++) {
		ofDrawBitmapString(ofToString(eyeFace.results[i]->track_number) + " , " + ofToString(eyeFace.results[i]->current_position.x)
			+ (eyeFace.results[i]->current_position.x > 0 ? " : ok" : "NOT OK"), 50, 50 + i * 10);
	}*/
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'd')
		debug = !debug;
	if (key == 'f')
		ofToggleFullscreen();
	if (key == 's') {
		if (settings.getNumTags("camDrawScale") > 0)
			settings.clearTagContents("camDrawScale");
		settings.setValue("camDrawScale", camDrawScaleSlider, 0);
		
		if (settings.getNumTags("videoOverlayOffset") > 0)
			settings.clearTagContents("videoOverlayOffset");
		settings.setAttribute("videoOverlayOffset", "x", videoOverlayOffsetX, 0);
		settings.setAttribute("videoOverlayOffset", "y", videoOverlayOffsetY, 0);

		if (settings.getNumTags("windowPosition") > 0)
			settings.clearTagContents("windowPosition");
		else settings.addTag("windowPosition");
		settings.setAttribute("windowPosition", "x", 0, 0);
		settings.setAttribute("windowPosition", "y", windowPositionY, 0);

		if (settings.getNumTags("windowSize") > 0)
			settings.clearTagContents("windowSize");
		else settings.addTag("windowSize");
		settings.setAttribute("windowSize", "x", windowSizeX, 0);
		settings.setAttribute("windowSize", "y", windowSizeY, 0);


		if (settings.getNumTags("videoOverlaySize") > 0)
			settings.clearTagContents("videoOverlaySize");
		settings.setAttribute("videoOverlaySize", "x", videoOverlaySizeX, 0);
		settings.setAttribute("videoOverlaySize", "y", videoOverlaySizeY, 0);


		settings.save("settings.xml");
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
/*void ofApp::windowResized(int w, int h){
//	ui_scale = (float)ofGetWidth() / detectionArea.x;
}*/

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
