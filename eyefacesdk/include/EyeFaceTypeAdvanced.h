////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
///                                                              ///
///    Expert API data header file of EyeFace SDK                ///
///   --------------------------------------------------------   ///
///    The interface described here is usable with the Expert    ///
///    license only.                                             ///
///                                                              ///
///    Eyedea Recognition, Ltd. (C) 2014, Nov 20th               ///
///                                                              ///
///    Contact:                                                  ///
///               web: http://www.eyedea.cz                      ///
///             email: info@eyedea.cz                            ///
///                                                              ///
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

#ifndef EYE_FACE_TYPE_ADVANCED_H
#define EYE_FACE_TYPE_ADVANCED_H

#include "EyeFaceType.h"

/*! Position  */
struct efposition
{    
    EfBoundingBox   bounding_box;   /*!< Bounding box of face detection.      */
    double          center_col;     /*!< Center column of detection.          */
    double          center_row;     /*!< Center row of detection.             */   
    double          height;         /*!< Scanning window height at detection. */ 
    double          width;          /*!< Scanning window width at detection.  */
};
typedef struct efposition EfPosition;


/*! Data related to a face detection. All data are related to a single frame, they are not aggregated as in EfVisualData. */
struct efdetection
{
    double          confidence;             /*!< Detection confidence factor.    */    
    EfPosition      position;               /*!< Position of detection in image. */ 
    double          time;                   /*!< Time of detection.              */
    
    EfMatrixUc *    image;                  /*!< The 8bit face image (the cropped face image is larger than the detection bounding-box). */
    
    int             image_landmark_used;    /*!< Boolean value telling if the above cropped image is face landmark normalized. */ 
    Ef2dPoints      landmarks;              /*!< Landmark points. Point 0-7 can be further stabilized using landmark_precise_use_precise (See the Developer's Guide). [0=FACE_CENTER 1=L_CANTHUS_R_EYE 2=R_CANTHUS_L_EYE 3=MOUTH_R 4=MOUTH_L 5=R_CANTHUS_R_EYE 6=L_CANTHUS_L_EYE 7=NOSE_TIP 8=L_EYEBROW_L 9=L_EYEBROW_C 10=L_EYEBROW_R 11=R_EYEBROW_L 12=R_EYEBROW_C 13=R_EYEBROW_R 14=NOSE_ROOT 15=NOSE_L 16=NOSE_R 17=MOUTH_T 18=MOUTH_B 19=CHIN]*/
    
    EfGender        gender;                 /*!< Gender class. */
    double          gender_response;        /*!< Classification response: -inf < male < -0.2 <= unknown <= 0.2 < female < inf. */
    int             age;                    /*!< Age in years. -1 if not recognized. */
    double          age_response;           /*!< Age response. */
    
    double          affine_mapping[6];      /*!< Affine mapping [Acc Acr Bc Arc Arr Br] from Image coordinates to source image coordinates. */
    
    double *        feat_id;                /*!< Features for identity recognition. */
    unsigned int    feat_id_length;         /*!< Identity feature length.           */
    
    double          angles[3];              /*!< Roll, Pitch, Yaw - only Yaw and Roll now used.     */
    double          landmark_yaw;           /*! Landmark yaw angle - estimation based on landmarks. */
};
typedef struct efdetection EfDetection;


/*! Detection result structure. */
struct efdetresult
{  
    int           num_detections;           /*!< Number of detections. */ 
    EfDetection * detection;                /*!< Array of detections.  */   
};
typedef struct efdetresult EfDetResult;


/*! Facial Landmark result structure. */
struct eflandmarkresult
{
    Ef2dPoints  points;                     /*!< Landmark points. Point 0-7 can be further stabilized using landmark_precise_use_precise (See the Developer's Guide). [0=FACE_CENTER 1=L_CANTHUS_R_EYE 2=R_CANTHUS_L_EYE 3=MOUTH_R 4=MOUTH_L 5=R_CANTHUS_R_EYE 6=L_CANTHUS_L_EYE 7=NOSE_TIP 8=L_EYEBROW_L 9=L_EYEBROW_C 10=L_EYEBROW_R 11=R_EYEBROW_L 12=R_EYEBROW_C 13=R_EYEBROW_R 14=NOSE_ROOT 15=NOSE_L 16=NOSE_R 17=MOUTH_T 18=MOUTH_B 19=CHIN]*/
    int         error;                      /*!< Error codes. 0 - OK. 1 - Landmark detection failed. 2 - Landmarks not accurate while face area is partially out of image.*/
};
typedef struct eflandmarkresult EfLandmarkResult;


/*! Gender & age result structure. All data are relate to a single frame. */
struct efgenderageresult
{
    EfGender    gender;                 /*!< Gender class. */
    double      gender_response;        /*!< Classification response: -inf < male < -0.2 <= unknown <= 0.2 < female < inf. */
    int         age;                    /*!< Age in years. -1 if not recognized. */
    double      age_response;           /*!< Age classification response. Currently not used */
};
typedef struct efgenderageresult EfGenderAgeResult;


/*! Gender result structure. All data are relate to a single frame. */
struct efgenderresult
{
    EfGender    gender;                 /*!< Gender class. */
    double      response;               /*!< Classification response: male < -0.2 <= unknown <= 0.2 < female. */
};
typedef struct efgenderresult EfGenderResult;


/*! Age result structure. All data are relate to a single frame. */
struct efageresult
{
    int     age;                        /*!< Age in years. -1 if not recognized. */ 
    double  response;                   /*!< Age classification response. Currently not used. */
};
typedef struct efageresult EfAgeResult;


////////////////////////////////////
// Public BETA of face detection in color images (see EyeFaceAdvanced.h):
/*! Image structure for 8 bit unsigned char image data. */
struct efmatrix3uc
{
    unsigned char *     data;           /*! Pointer to row-wise 8-bit image data. */
    int                 length;         /*! Number of pixels. */
    int                 alloc_length;   /*! allocated size. */
    int                 no_rows;        /*! Number of rows, i.e. image height. */
    int                 no_cols;        /*! Number of columns, i.e. image width. */
    
    Ef_Bool             data_alloc;     /*! Flag indicating whether the field data was dynamically allocated. */
    Ef_Bool             struct_alloc;   /*! Flag indicating whether the structure was dynamically allocated. */
    
    unsigned char **    row_data;       /*! Array of pointers to first pixel in a given row. Can be used for row&col indexing: matrix.row_data[row][col]. */
};
typedef struct efmatrix3uc EfMatrix3Uc;



// /////////////////////////////////////////////////////////////// //
//                                                                 //
// EyeFace Expert Api Functions typedefs (see EyeFaceAdvanced.h)   //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
// face detection
typedef EfDetResult*        (*fcn_efRunFaceDetector)(EfMatrixUc*, void*);
typedef void                (*fcn_efFreeDetResult)(EfDetResult*);  

// Public BETA - Color face detection
typedef EfDetResult*        (*fcn_efRunFaceDetectorRGB)(EfMatrix3Uc*, void*);
typedef EfMatrix3Uc*        (*fcn_efReadImage3Uc)(const char*);
typedef EfMatrix3Uc*        (*fcn_efAllocImage3Uc)(int, int);
typedef void                (*fcn_efFreeImage3Uc)(void*);

// tracker
typedef int                 (*fcn_efUpdateTrackState)(void*,void*,double);
// face extraction 
typedef int                 (*fcn_efRunFaceImageExtraction)(EfMatrixUc*, EfDetection*, void*, EfLandmarkResult*);
// id extraction
typedef int                 (*fcn_efRunFaceIDFeatureExtraction)(EfDetection*, const void *);
// landmarks
typedef EfLandmarkResult*   (*fcn_efRunFaceLandmark)(EfMatrixUc*, EfDetection*, void*);
typedef void                (*fcn_efFreeLandmarkResult)(EfLandmarkResult*); 
// gender & age
typedef EfGenderAgeResult*  (*fcn_efRunFaceGenderAge)(EfDetection*, void*);
typedef void                (*fcn_efFreeGenderAgeResult)(EfGenderAgeResult*); 
// gender
typedef EfGenderResult*     (*fcn_efRunFaceGender)(EfDetection*, void*);
typedef void                (*fcn_efFreeGenderResult)(EfGenderResult*); 
// age
typedef EfAgeResult*        (*fcn_efRunFaceAge)(EfDetection*, void*);
typedef void                (*fcn_efFreeAgeResult)(EfAgeResult*); 

typedef int                 (*fcn_efIDCompareFeatureVectors)(const double *, const double *, const void *, double *);



#endif