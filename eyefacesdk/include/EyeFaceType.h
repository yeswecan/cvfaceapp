////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
///                                                              ///
///    Standard API data header file of EyeFace SDK              ///
///   --------------------------------------------------------   ///
///    The interface described here is usable with both the      ///
///    Standard and the Expert license.                          ///
///                                                              ///
///    Eyedea Recognition, Ltd. (C) 2014, Nov 20th               ///
///                                                              ///
///    Contact:                                                  ///
///               web: http://www.eyedea.cz                      ///
///             email: info@eyedea.cz                            ///
///                                                              ///
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

#ifndef EYE_FACE_TYPE_H
#define EYE_FACE_TYPE_H

#define efPrintError(A,B,C) eyePrintError((char*)A,(char*)B,(char*)C)

// /////////////////////////////////////////////////////////////// //
//                                                                 //
// EYEFACE STANDARD API - C compatible                             //
//                                                                 //
// /////////////////////////////////////////////////////////////// //

/*! Boolean data type. */
typedef enum
{
    EF_FALSE    = 0,        /*!< Boolean false value. */
    EF_TRUE     = 1         /*!< Boolean true value.  */
}Ef_Bool;

/*! Set of 2D points structure. */
struct ef2dpoints
{
    int         length;     /*!< Number of points in the set.  */
    double *    rows;       /*!< Row coordinates of points.    */
    double *    cols;       /*!< Column coordinates of points. */
};
typedef struct ef2dpoints Ef2dPoints;

/*! Set of 2D points structure, for landmarks in wrapper interfaces. DEPRECATED.*/
struct ef2dpoints8
{
    int         length;     /*!< Number of valid points in the set. */
    double      rows[32];   /*!< Row coordinates of points.         */
    double      cols[32];   /*!< Column coordinates of points.      */
};
typedef struct ef2dpoints8 Ef2dPoints8;

/*! Image structure for 8 bit unsigned char image data. */
struct efmatrixuc
{
    unsigned char *     data;           /*!< Pointer to row-wise 8-bit image data, index = col + row * width.  */
    int                 length;         /*!< Number of pixels. */
    int                 alloc_length;   /*!< Allocated size.   */
    int                 no_rows;        /*!< Number of rows, i.e. image height.   */
    int                 no_cols;        /*!< Number of columns, i.e. image width. */
 
    Ef_Bool             data_alloc;     /*!< Flag indicating whether the field data was dynamically allocated. */    
    Ef_Bool             struct_alloc;   /*!< Flag indicating whether the structure was dynamically allocated.  */
    
    unsigned char **    row_data;       /*!< Array of pointers to first pixel in a given row. Can be used for row&col indexing: matrix.row_data[row][col]. */
};
typedef struct efmatrixuc EfMatrixUc;

/*! Bounding-box coordinates of a detection area */
struct efboundingbox
{
    int top_left_col;       /*!< Column index of top left corner of BB.  */
    int top_left_row;       /*!<  Row index of top left corner of BB.    */
    int top_right_col;      /*!< Column index of top right corner of BB. */
    int top_right_row;      /*!< Row index of top right corner of BB.    */
    int bot_left_col;       /*!< Column index of bot left corner of BB.  */   
    int bot_left_row;       /*!< Row index of bot left corner of BB.     */
    int bot_right_col;      /*!< Column index of bot right corner of BB. */
    int bot_right_row;      /*!< Row index of bot right corner of BB.    */
}; 
typedef struct efboundingbox EfBoundingBox;

///////////////////////////////////////////////////////////////////////////
// !!! WARNING: THIS ENUM HAS BEEN CHANGED BETWEEN v3.9.0 and 3.11.0 !!! //
///////////////////////////////////////////////////////////////////////////
/*! Gender enum type. */
typedef enum
{
    EF_GENDER_MALE   = -1,  /*!< Male value. */    
    EF_GENDER_NONE   =  0,  /*!< Non-gender value. */
    EF_GENDER_FEMALE =  1   /*!< Female value. */
}EfGender;

/*! Structure containing visualisation data for a given track. */
struct efvisualdata
{
    int             track_number;               /*!< Track unique number. Track is a space-time aggregation of face detecions. */
    int             id;                         /*!< Person identity unique number based on face recognition, 0 if not known yet.*/
                                                
    EfBoundingBox   current_position;           /*!< Face position. Internally aggregated over past frames. */
    double          energy;                     /*!< Fade-out energy: 1 if the face was detected in the current frame, less than 1 if not. This number is used for alpha channel of bounding-box visualisation (fade-out). */
    
    EfGender        gender;                     /*!< Face gender. Internally aggregated over all past face frames. */
    double          gender_confidence;          /*!< Classification response: -1 <= male < -0.2 <= unknown <= 0.2 < female <= 1. Internally aggregated over past frames. */
    double          age_response;               /*!< Age clasification response, currently NOT USED. */ 
    int             age;                        /*!< Age in years (or -1 for unknown). Internally aggregated over all past face frames. */
    
    int             logged;                     /*!< Track status: 0 - a not verified track (face detected just in one frame), 1 or 2 - a verified track, face detected in 2 frames at least. */
    
    Ef2dPoints8     landmarks;	                /*!< Landmark points. Point 0-7 can be further stabilized using landmark_precise_use_precise (See the Developer's Guide). [0=FACE_CENTER 1=L_CANTHUS_R_EYE 2=R_CANTHUS_L_EYE 3=MOUTH_R 4=MOUTH_L 5=R_CANTHUS_R_EYE 6=L_CANTHUS_L_EYE 7=NOSE_TIP 8=L_EYEBROW_L 9=L_EYEBROW_C 10=L_EYEBROW_R 11=R_EYEBROW_L 12=R_EYEBROW_C 13=R_EYEBROW_R 14=NOSE_ROOT 15=NOSE_L 16=NOSE_R 17=MOUTH_T 18=MOUTH_B 19=CHIN]*/
    double          curr_position_x;            /*!< 2D real-world position - X axis (left-right). Internally aggregated over past frames. */
    double          curr_position_y;            /*!< 2D real-world position - Y axis (forward-backward). Internally aggregated over past frames.*/
    double          angles[3];                  /*!< Roll, Pitch, Yaw - only Yaw and Roll now used. */
    
    double          start_time;                 /*!< Track start time [seconds]. */
    double          total_time;                 /*!< Track duration [seconds].   */
    double          attention_time;             /*!< Estimate of time [seconds] the person looked into camera. (See the Developer's Guide)*/
    int             attention_now;              /*!< Estimate of whether the person looks in the camera in current frame. */
      
    int             detection_index;            /*!< Use in Advanced API only if dEnergy == 1! Tells the index to EfDetResult structure outputted by efRunFaceDetector, so that EfVisualData can be matched with EfDetection. */
};
typedef struct efvisualdata EfVisualData;

/*! Structure containg visualisation data for the last frame. */
struct efvisualoutput
{    
    int             num_vis;                    /*!< Number of face tracks.               */
    EfVisualData *  vis_data;                   /*!< Array face track visualisation data. */  
};
typedef struct efvisualoutput EfVisualOutput;

/*! Structure containing information regarding finished tracks */
struct eftrackfinalinfo
{
    int     device_id;                       /*!< Integer assigned to a camera by a user.                  */        
    int     track_num;                       /*!< Unique identifier of a track.                            */
    
    double  start_time;                      /*!< Time of tracks creation, UNIX/POSIX format. Initialization time in POSIX + time inputted to efMain or efUpdateTrackState. */
    double  end_time;                        /*!< Time of tracks closure, UNIX/POSIX format.               */
    
    int     pid;                             /*!< Person ID identifying person contained in the track.     */
    double  gender;                          /*!< Gender response. Internally aggregated over track.       */
    int     age;                             /*!< Age in years. Internally aggregated over track.          */
    
    double  min_radius;                      /*!< Min diagonal size of detection bounding box.             */
    double  max_radius;                      /*!< Max diagonal size of detection bounding box.             */
    double  mean_radius;                     /*!< Mean diagonal size of detection bounding box.            */
    
    double  attention_time;                  /*!< total attention time in seconds.                         */
};
typedef struct eftrackfinalinfo EfTrackFinalInfo;



// /////////////////////////////////////////////////////////////// //
//                                                                 //
// EyeFace Standard API (see EyeFace.h)                            //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
typedef EfMatrixUc*     (*fcn_efReadImageUc)(const char*);
typedef EfMatrixUc*     (*fcn_efAllocImageUc)(int, int);
typedef EfMatrixUc*     (*fcn_efWriteImageUc)(EfMatrixUc*, int, const char*);
typedef EfMatrixUc*     (*fcn_efEncloseImageUc)( unsigned char*, int, int );
typedef void            (*fcn_efFreeImageUc)(void*);

typedef void *          (*fcn_efInitEyeFace)(char*,char*,char*,char*);
typedef void            (*fcn_efShutdownEyeFace)(void*);
typedef void            (*fcn_efFreeEyeFaceState)(void*);
typedef void            (*fcn_efClearEyeFaceState)(void*);
typedef int             (*fcn_efMain)( unsigned char*,int,int,int,EfBoundingBox*,void*,double);
typedef EfVisualOutput* (*fcn_efGetVisualOutput) (void*,double,int );
typedef void            (*fcn_efFreeVisualOutput)(EfVisualOutput*);
typedef int             (*fcn_efGetLibraryVersion)();
typedef int             (*fcn_efGetTrackFinalInfoOutput)(void*, EfTrackFinalInfo***);
typedef void            (*fcn_efFreeTrackFinalInfoOutput)(EfTrackFinalInfo**, int);
typedef int             (*fcn_efSetEyeFaceFlagParams)(void*,int);
typedef int             (*fcn_efGetEyeFaceFlagParams)(void*);



// /////////////////////////////////////////////////////////////// //
//                                                                 //
// SENTINEL LDK LICENSING API INFO DEFINITIONS                     //
// (Previously named HASP API)                                     //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
/*! HASP time structure. */
struct efhasptime
{
    int year;                /*!< Year.         */
    int month;               /*!< Month.        */
    int day;                 /*!< Day of month. */
    int hour;                /*!< Hour.         */
    int minute;              /*!< Minute.       */
    int second;              /*!< Second        */
};
typedef struct efhasptime EfHaspTime;

/*! HASP product feature structure. */
struct efhaspfeatureinfo
{
    char        license_type[32];               /*!< String describing the license type. Possible values are: perpetual, trial, expiration, timeperiod, executions. */
    int         usable;                         /*!< Flag indicating whether the product feature is usable. */
    EfHaspTime  start_time;                     /*!< License activation time, only for "trial" or "timeperiod" licenses, otherwise 0. */
    EfHaspTime  expiration_time;                /*!< License expiration time. */
    int         total_time;                     /*!< Time period length in seconds for which the license is valid, only for "trial" or "timeperiod" licenses! */
    
    int         max_logins;                     /*!< Max simultaneous logins. */
    int         current_logins;                 /*!< Number of current logins. */
            
    char        xml_feature_info[1024];         /*!< Feature info in xml format. */
};
typedef struct efhaspfeatureinfo EfHaspFeatureInfo;

/*! HASP software license key structure. */
struct efhaspkeyinfo
{
    long long           id;                     /*!< Hasp key id number. */
    char                xml_key_info[1024];     /*!< Hasp key info in xml format. */
    int                 trial;                  /*!< Flag indicating whether a key is a trial key or a purchased key. */
    int                 is_present;             /*!< Flag indicating whether a key is present. */
    
    int                 num_feats;              /*!< Number of features present. */
    int *               feat_ids;               /*!< Feature IDs. */
    EfHaspFeatureInfo * feats;                  /*!< Info about features present. */  
};
typedef struct efhaspkeyinfo EfHaspKeyInfo;

// Type declaration of pointers to Sentinel LDK API functions.
typedef int        (*fcn_efGetLicenseExpirationDate)(EfHaspTime*, int);
typedef long long  (*fcn_efHaspGetCurrentLoginKeyId)();
typedef long long  (*fcn_efHaspGetCurrentLoginKeyIdFromState)(void*);
typedef int        (*fcn_efHaspGetExpirationDate)(long long, EfHaspTime*);
typedef long long* (*fcn_efHaspGetKeyIdsFromInfo)(int*);
typedef int        (*fcn_efHaspGetKeyInfo)(EfHaspKeyInfo*,long long);
typedef int        (*fcn_efHaspGetSessionKeyInfo)(EfHaspKeyInfo*);
typedef int        (*fcn_efHaspWriteC2VtoFile)(char*);
typedef int        (*fcn_efHaspWriteAllC2VtoOneFile)(char*);
typedef int        (*fcn_efHaspActivateV2C)(char*,char*);

// /////////////////////////////////////////////////////////////// //
//                                                                 //
// CONNECTION TO LOG SERVER                                        //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
/*! Status of a connection to log server. */
struct efconnstate
{
    int     server_is_reachable;    /*!< 0-not-reachable 1-reachable          */
    int     num_messages_ok;        /*!< number of messages successfully sent */
    int     num_messages_failed;    /*!< number of messages failed to send    */
    char    camera_name[32];        /*!< Camera name (license key id or demo) */
};
typedef struct efconnstate EfConnState;

// Type declaration of pointers to EyeFace SDK log server API functions.
typedef int             (*fcn_efGetConnectionState)(void*, EfConnState*);
typedef int             (*fcn_efCurlSendTrackInfo)(void*);
typedef int             (*fcn_efCurlSendPing)(void*);

#endif

