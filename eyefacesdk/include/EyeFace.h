////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
///                                                              ///
///    Standard API header file of EyeFace SDK                   ///
///   --------------------------------------------------------   ///
///    The interface described here is usable with both the      ///
///    Standard and the Expert license.                          ///
///                                                              ///
///    Eyedea Recognition, Ltd. (C) 2014, Nov 20th               ///
///                                                              ///
///    Contact:                                                  ///
///               web: http://www.eyedea.cz                      ///
///             email: info@eyedea.cz                            ///
///                                                              ///
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

#ifndef EYE_FACE_H
#define EYE_FACE_H

#include "EyeFaceType.h"  // Types used in EyeFace SDK

// EyeFace SDK header version
#define EYEFACE_VERSION_NUMBER 30110000   

#if defined(WIN32) || defined(_WIN32) || defined(_DLL) || defined(_WINDOWS_) || defined(_WINDLL)
// windows expoerts
# define EYEFACE_FUNCTION_PREFIX extern "C" __declspec(dllexport)
#else
// so library on unix and mac osx
# define EYEFACE_FUNCTION_PREFIX extern "C" __attribute__ ((visibility ("default"))) 
#endif

/*! \defgroup EyeFace EyeFace API.
 @{ 
*/
/*! @} */

/*! \addtogroup EyeFace
 @{
*/

// /////////////////////////////////////////////////////////////// //
//                                                                 //
// EYE-FACE STANDARD API:  FOR IMAGE SEQUENCES                     //
//                                                                 //
// /////////////////////////////////////////////////////////////// //  

/*! \defgroup Standard  EyeFace Standard API
 @{
*/

/*! \fn EfMatrixUc * efAllocImageUc(int num_rows, int num_cols)
  \brief  Allocates 8-bit image bitmap structure used by EyeFace SDK.
  \param  num_rows Number of rows.
  \param  num_cols Number of columns.
  \return Pointer to EfMatrixUc, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX EfMatrixUc * efAllocImageUc(int num_rows, int num_cols);

/*! \fn EfMatrixUc * efEncloseImageUc(unsigned char * data, int num_rows, int num_cols)
 \brief  Creates a EfMatrixUc wrapper for user data 8-bit image bitmap. The data are not copied.
 \param  data    Pointer to grayscale image data.
 \param  num_rows Number of rows.
 \param  num_cols Number of columns.
 \return Pointer to EfMatrixUc, NULL on failure.
 */
EYEFACE_FUNCTION_PREFIX EfMatrixUc * efEncloseImageUc(unsigned char * data, int num_rows, int num_cols);

/*! \fn EfMatrixUc * efReadImageUc(const char * filename)
  \brief  Reads an image from file (Supports jpegs, tiffs and pngs).
  \param  filename Image filename.
  \return Pointer to EfMatrixUc, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX  EfMatrixUc * efReadImageUc(const char * filename);

/*! \fn int efWriteImageUc(EfMatrixUc * image, int quality, const char * filename)
  \brief  Writes an image to file (Supported jpegs).
  \param image Pointer to EfMatrixUc image.
  \param quality Quality of saved image, 0-100.
  \param filename Image filename.
  \return -1 on failure.
*/
EYEFACE_FUNCTION_PREFIX int efWriteImageUc(EfMatrixUc * image, int quality, const char * filename);

/*! \fn void efFreeImageUc(EfMatrixUc * image)
  \brief  Free EfMatrixUc image structure allocated by efAllocImageUc and efEncloseImageUc.
  \param  image Pointer to EfMatrixUc image structure.
*/
EYEFACE_FUNCTION_PREFIX void efFreeImageUc(EfMatrixUc * image);

/*! \fn void * efInitEyeFace(char * module_dir, char * log_dir, char * ini_dir, char * ini_file)
  \brief  Initializes EyeFace SDK engine and loads detection and recognition models from eyefacesdk/models.
  \param  module_dir Path to a directory containg the "lib/EyeFace.dll" ["lib/libeyefacesdk*.so" on Linux] file and "models" directory.
  \param  log_dir Path to a directory where the ouput log files will be saved.
  \param  ini_dir Path to a directory containg configuration ini file.
  \param  ini_file Config file name (e.g. config.ini).
  \return Returns a pointer to EyeFace module state, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX  void * efInitEyeFace(char * module_dir, char * log_dir, char * ini_dir, char * ini_file);

/*! \fn void * efShutdownEyeFace(void * module_state)
  \brief  Flushes tracking buffers to log and trackinfo outputs before EyeFace engine termination.
  \param  module_state Pointer to EyeFace module state created by efInitEyeFace().
*/
EYEFACE_FUNCTION_PREFIX void efShutdownEyeFace(void * module_state);

/*! \fn void efFreeEyeFaceState(void * module_state)
  \brief  Frees EyeFace engine. Releases the license sessions.
  \param  module_state Pointer to EyeFace module state created by efInitEyeFace().
*/
EYEFACE_FUNCTION_PREFIX void efFreeEyeFaceState(void * module_state);

/*! \fn void efClearEyeFaceState(void * module_state)
  \brief  Clears/resets state structure without deallocation.
  \param  module_state Pointer to a state created by efInitEyeFace().
*/
EYEFACE_FUNCTION_PREFIX void efClearEyeFaceState(void * module_state);


// Bits which enable or disable modules
/*! Bitmask for efSetEyeFaceFlagParams, age. */
#define FLAG_USE_AGE     512   
/*! Bitmask for efSetEyeFaceFlagParams, gender. */
#define FLAG_USE_GENDER  1024  
/*! Bitmask for efSetEyeFaceFlagParams, id. */
#define FLAG_USE_ID	  2048

/*! \fn int efSetEyeFaceFlagParams(void * module_state, int flag)
  \brief  Sets the flag parameters (gender on/off, age on/off) etc.
  \param  module_state Pointer to a state created by efInitEyeFace().
  \param  flag 32 bit word coding the flag params, see this header file.
  \return Zero on success, error code on failure.
*/
EYEFACE_FUNCTION_PREFIX int efSetEyeFaceFlagParams(void * module_state, int flag);


/*! \fn int efGetEyeFaceFlagParams(void * module_state)
  \brief  Gets the flag parameters (gender on/off, age on/off) etc.
  \param  module_state Pointer to a state created by efInitEyeFace().
  \return 32 bit word coding the flag params or -1 on error.
*/
EYEFACE_FUNCTION_PREFIX int efGetEyeFaceFlagParams(void * module_state);


/*! \fn int efMain(const unsigned char * image, int width, int height, int vertical_flip, EfBoundingBox * bounding_box, void * module_state, double frame_time)
  \brief  Processes current image, i.e. detects faces, runs age/gender/id recognition and tracks the detected faces. For a single video-sequence images only.
  \param  image A pointer to grayscale input image (a 1-D array of unsigned char's, stored as index = col + image_width*row).
  \param  width Input image width.
  \param  height Input image height.
  \param  vertical_flip Tag indicating whether to flip input image vertically before processing. (The first byte should correspond to the top-left pixel). 
  \param  bounding_box Bonding-box sellecting the active image area where the detection will take place (NULL for whole image).
  \param  module_state Pointer to EyeFace module state ( created by efInitEyeFace() ).
  \param  frame_time Current frame time from camera start in seconds (i.e. the first frame time MUST be 0.0). The frame time is used for tracking to evaluate speed of moving objects. 
  \return Zero on success, error code on failure.
*/
EYEFACE_FUNCTION_PREFIX  int   efMain(const unsigned char * image, 
                              int width, int height,
                              int vertical_flip,
                              EfBoundingBox * bounding_box,
                              void* module_state,
                              double frame_time);


/*! \fn int efGetTrackFinalInfoOutput(void * module_state, EfTrackFinalInfo *** track_final_info_array)
    \brief Fills the array of pointers to EfTrackFinalInfo structures with information about currently ended tracks.  
    \param module_state Pointer to state structure received by efInitEyeFace().
    \param track_final_info_array Address of a pointer to a dynamic array of pointers to EfTrackFinalInfo.
    \return -1 on error, otherwise number of currently ended tracks (length of track_final_info_array).
*/
EYEFACE_FUNCTION_PREFIX int efGetTrackFinalInfoOutput(void * module_state, EfTrackFinalInfo *** track_final_info_array);


/*! \fn int efFreeTrackFinalInfoOutput(EfTrackFinalInfo ** track_final_info_array, int length)
    \brief Frees the dynamic memory allocated by efGetTrackFinalInfoOutput() 
    \param track_final_info_array Pointer to dynamic array of pointers to EfTrackFinalInfo.
    \param length Length of the array pointed to by track_final_info_array, return value of efGetTrackFinalInfoOutput().
*/
EYEFACE_FUNCTION_PREFIX void efFreeTrackFinalInfoOutput(EfTrackFinalInfo ** track_final_info_array, int length);


/*! \fn EfVisualOutput * efGetVisualOutput(void * module_state, double min_det_stability, int visible_at_least)
  \brief  Gets the results to be visualized on a given input frame.
  \param  module_state Pointer to EyeFace module state (created by efInitEyeFace()).
  \param  min_det_stability Only tracks with minimum aggregated energy > min_det_stability are returned (0.0-1.0).
  \param  visible_at_least Only tracks with number of consecutive detections > visible_at_least are returned.
  \return Returns a pointer to state, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX EfVisualOutput * efGetVisualOutput(void* module_state, double min_det_stability, int visible_at_least);


/*! \fn void efFreeVisualOutput(EfVisualOutput * visual_output)
  \brief  Frees EfVisualOutput * received by efGetVisualOutput().
  \param  visual_output Pointer to EfVisualOutput.
*/
EYEFACE_FUNCTION_PREFIX void efFreeVisualOutput(EfVisualOutput * visual_output);


/*! \fn int efGetLibraryVersion()
  \brief  Return the version of the library, for example if v3.11.0 then the return value is 30110000.
  \return Version number.
*/
EYEFACE_FUNCTION_PREFIX int efGetLibraryVersion();


// /////////////////////////////////////////////////////////////// //
//                                                                 //
// Connection to log server                                        //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
/*! \fn int efGetConnectionState(void * module_state, EfConnState * con_state)
  \brief Get status of connection to log-server
  \param module_state EyeFace module state.
  \param con_state Adress of EfConnState object, where to save results. 
*/
EYEFACE_FUNCTION_PREFIX int efGetConnectionState(void * module_state, EfConnState * conn_state);

/*! \fn void efCurlSendTrackInfo(void * module_state)
  \brief Sends the track info to log server. Run after efTrackUpdate().
  \param module_state EyeFace module state.
*/
EYEFACE_FUNCTION_PREFIX int efCurlSendTrackInfo(void * module_state);

/*! \fn void efCurlSendPing(void * module_state)
  \brief Send ping to log server.
  \param module_state EyeFace module state.
*/
EYEFACE_FUNCTION_PREFIX int efCurlSendPing(void * module_state);

/*! @} */

/*! @} */

/*! \addtogroup EyeFace
 @{
*/

/*! \defgroup Licensing  HASP API
 @{
*/
// /////////////////////////////////////////////////////////////// //
//                                                                 //
// Safenet(R) Sentinel LDK license key API                         //
// Define HASP_SECURE in your application to use it.               //
//                                                                 //
// /////////////////////////////////////////////////////////////// //
#ifdef HASP_SECURE

/*! \fn long long * efHaspGetKeyIdsFromInfo(int * num_keys, int product_id)
\brief Gets number of recognised HASP keys and their IDs.
\param num_keys Pointer to int where to write number of recognised keys.
\param  product_id Product id, see eyeface-hasp.h, use EYEDEA_PROD_EYEFACE_STANDARD or EYEDEA_PROD_EYEFACE_EXPERT.
\return Pointer to allocated array of key IDs.
*/
EYEFACE_FUNCTION_PREFIX long long * efHaspGetKeyIdsFromInfo(int * num_keys, int product_id);

/*! \fn int efHaspGetExpirationDate(long long key_id, EfHaspTime * exp_time)
  \brief  Gets an expiration date for the given HASP key ID.
  \param  key_id ID of the key in question.
  \param  exp_time EfHaspTime structure pointer, for expiration date output. 
  \return 1 for valid license, 0 for not valid.
*/
EYEFACE_FUNCTION_PREFIX int efHaspGetExpirationDate(long long key_id, EfHaspTime * exp_time);

/*! \fn long long efHaspGetCurrentLoginKeyId()
  \brief Gets a single license key number  which will be used at startup by default. Must be used prior to efInitEyeFace() to work. The actual key number used later may vary, use efHaspGetCurrentLoginKeyIdFromState() after efInitEyeFace().
  \return Key number of key containing EyeFace SDK license.
*/
EYEFACE_FUNCTION_PREFIX long long efHaspGetCurrentLoginKeyId();

/*! \fn long long efHaspGetCurrentLoginKeyIdFromState(void * module_state)
  \brief Gets a single license key number  which is currently used by an initialized engine of EyeFace SDK. 
  \param module_state EyeFace module state. Returned by efInitEyeFace()
  \return Key number of key containing EyeFace SDK license.
*/
EYEFACE_FUNCTION_PREFIX long long efHaspGetCurrentLoginKeyIdFromState(void * module_state);

/*! \fn int efHaspGetKeyInfo(EfHaspKeyInfo * hasp_key_info,long long key_id)
  \brief Gets info for a key specified by its ID.
  \param hasp_key_info Pointer to EfHaspKeyInfo structure containg hasp key info data.
  \param key_id Key ID.
  \return Zero on success, error code on failure. 
*/
EYEFACE_FUNCTION_PREFIX int efHaspGetKeyInfo(EfHaspKeyInfo * hasp_key_info,long long key_id);

/*! \fn int efHaspGetSessionKeyInfo(EfHaspKeyInfo * hasp_key_info)
  \brief  Gets current session feature info of all features. (If there are more Sentinel LDK keys for one feature, licenses from different keys can be used.)  Must be used prior to efInitEyeFace() to work.
  \param  hasp_key_info Pointer to EfHaspKeyInfo structure containg feature info data.
  \return Zero on success, error code on failure. 
*/
EYEFACE_FUNCTION_PREFIX int efHaspGetSessionKeyInfo(EfHaspKeyInfo * hasp_key_info);

/*! \fn int efHaspWriteC2VtoFile(const char * c2v_path)
  \brief  Creates C2V (client to vendor) file and writes C2V info into it. The created filename has the following format "[Key_ID].c2v".
  \param  c2v_path Path where the C2V file will be created. 
  \return Zero on success, error code on failure. 
*/
EYEFACE_FUNCTION_PREFIX int efHaspWriteC2VtoFile(const char * c2v_path);

/*! \fn int efHaspWriteAllC2VtoOneFile(const char * filename)
  \brief  Writes c2v info of all recognized keys to one common file.
  \param  filename Output filename.
  \return Zero on success.
*/
EYEFACE_FUNCTION_PREFIX int efHaspWriteAllC2VtoOneFile(const char * filename);

/*! \fn int efHaspActivateV2C(const char * v2c_filename, const char * ack_filename)
  \brief  Activates V2C license received from a vendor. Creates aknowledge data file which could be send to a vendor to regiter the license activation.
  \param  v2c_filename V2C filename.
  \param  ack_filename Aknowledge data filename.
  \return Zero on success, error code on failure. 
*/
EYEFACE_FUNCTION_PREFIX int efHaspActivateV2C(const char * v2c_filename, const char * ack_filename);

/*! \fn int efGetLicenseExpirationDate(EfHaspTime * exp_date, int product_id)
  \brief  Gets the expiration date of the the license for the given product. Must be used prior to efInitEyeFace() to work.
  \param  exp_date Output of expiration date.
  \param  product_id Product id, see eyeface-hasp.h, use EYEDEA_PROD_EYEFACE_STANDARD or EYEDEA_PROD_EYEFACE_EXPERT.
  \return Zero on success, error code on failure. 
*/
EYEFACE_FUNCTION_PREFIX  int efGetLicenseExpirationDate(EfHaspTime * exp_date, int product_id);
#endif

/*! @} */

/*! @} */
  

/////////////////////////////////////////////////////////////////////////
// Multiplatform explicit linking defines ///////////////////////////////
/////////////////////////////////////////////////////////////////////////
#if defined(WIN32) || defined(_WIN32) || defined(_DLL) || defined(_WINDOWS_) || defined(_WINDLL)
// WIN32 Platform //

// DLL types //
# ifdef EYE_DLLIMPORT
#  define EF_DLL_TYPE dllimport
# else
#  define EF_DLL_TYPE dllexport
# endif

// C code extern //
#if defined(CPP) || defined(__cplusplus) || defined(c_plusplus)
#  define EF_SHFCN_ATTR extern "C" __declspec(EF_DLL_TYPE)
#  define EF_FCN_ATTR extern "C"
# else
#  define EF_SHLIB_ATTR __declspec(EF_DLL_TYPE)
#  define EF_FCN_ATTR
# endif

// Standard includes //
# include <windows.h>
# include <direct.h>
# include <io.h>

// Shared lib. prefix //
# define EF_SHLIB_PREFIX ""
// Shared lib. suffix //
# define EF_SHLIB_SUFFIX ".dll"

// type definition of shared lib. handle //
typedef HMODULE ef_shlib_hnd;

// Shared lib. open and function load routines. //
# define EF_OPEN_SHLIB(shlib_hnd, shlib_filename) (shlib_hnd = LoadLibrary(shlib_filename))

# define EF_LOAD_SHFCN(shfcn_ptr, shfcn_type, shlib_hnd, shfcn_name) (shfcn_ptr = (shfcn_type) GetProcAddress(shlib_hnd, shfcn_name))

# define EF_FREE_LIB(shlib_hnd) FreeLibrary(shlib_hnd)

# define EF_SHERROR ""


#else //////////////////////////////////////////////////////////////////////////////////////////////

// UNIX Platforms //

// C code extern //
#if defined(CPP) || defined(__cplusplus) || defined(c_plusplus)
#  define EF_SHFCN_ATTR extern "C"
#  define EF_FCN_ATTR extern "C"
# else
#  define EF_SHFCN_ATTR
#  define EF_FCN_ATTR
# endif

// Standard includes //
# include <dlfcn.h>

// Windows like _MAX_PATH macro definition. //
#ifndef _MAX_PATH
# include <limits.h>
# ifndef PATH_MAX
#  define PATH_MAX NAME_MAX+1
# endif
# define _MAX_PATH PATH_MAX
#endif

// Shared lib. prefix //
# define EF_SHLIB_PREFIX "lib"
// Shared lib. suffix //
# define EF_SHLIB_SUFFIX ".so"

// type definition of shared lib. handle //
typedef void* ef_shlib_hnd;

// Shared lib. open and function load routines. //
# define EF_OPEN_SHLIB(shlib_hnd, shlib_filename) (shlib_hnd = dlopen(shlib_filename,RTLD_LAZY))

# define EF_LOAD_SHFCN(shfcn_ptr, shfcn_type, shlib_hnd, shfcn_name) (shfcn_ptr = (shfcn_type) dlsym(shlib_hnd, shfcn_name))

# define EF_FREE_LIB(shlib_hnd) dlclose(shlib_hnd)

# define EF_SHERROR dlerror()

#endif // Multiplatform defines //
/////////////////////////////////////////////////////////////////////////////////////////////////

#endif

